FROM node:latest

ADD emsn emsn
WORKDIR emsn
RUN npm install
CMD npm run dev -- --host 0.0.0.0 --port 80 --public interactive.emsn.eu
EXPOSE 80
